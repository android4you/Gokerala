package com.android4you.gokerala.ui.widget;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android4you.gokerala.R;
import com.android4you.gokerala.ui.models.ItemVerticalModel;

import java.util.List;


/**
 * Created by Manu on 11/14/2017.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> implements View.OnClickListener {

    private List<ItemVerticalModel> mItems;
    private OnItemClickListener mOnItemClickListener;

    public GalleryAdapter(List<ItemVerticalModel> items) {
        this.mItems = items;
    }

    public GalleryAdapter setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
        return this;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_row, parent, false);
        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int temp = position;
        if (position >= mItems.size()) {
            position = position % mItems.size();
        }
        int item = mItems.get(position).getIcon();
        holder.icon.setImageResource(item);
        holder.title.setText(mItems.get(position).getTitle());
        holder.itemView.setTag(temp);
    }

    public static int LOOPS_COUNT = 1000;

    @Override
    public int getItemCount() {
        if (mItems != null && mItems.size() > 0) {
            return mItems.size() * LOOPS_COUNT;
        } else {
            return 1;
        }
    }

    @Override
    public void onClick(final View v) {
        if (mOnItemClickListener != null) {
            int position = (int) v.getTag();
            mOnItemClickListener.onItemClick(v, position);
        }
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView icon;
        public TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.imageView);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}