package com.android4you.gokerala.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android4you.gokerala.R;
import com.android4you.gokerala.ui.fragments.FragmentAboutus;
import com.android4you.gokerala.ui.fragments.FragmentContactus;
import com.android4you.gokerala.ui.fragments.FragmentDrawer;
import com.android4you.gokerala.ui.fragments.FragmentFeedback;
import com.android4you.gokerala.ui.fragments.home.FragmentDetails;
import com.android4you.gokerala.ui.fragments.home.FragmentHome;


/**
 * Created by Manu on 11/13/2017.
 */

public class HomeActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private static String TAG = HomeActivity.class.getSimpleName();

    private RelativeLayout mToolbar;
    private FragmentDrawer drawerFragment;
    private ImageView sidemenuButton;
    private DrawerLayout drawerLayout;
    private TextView headerTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mToolbar =  findViewById(R.id.toolbar);
        headerTitle = findViewById(R.id.headerTitle);
        sidemenuButton =  findViewById(R.id.iv_icon);
        drawerLayout =  findViewById(R.id.drawer_layout);
        sidemenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.END);
            }
        });

      //  setSupportActionBar(mToolbar);
      //  getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer,drawerLayout , null);
        drawerFragment.setDrawerListener(this);

        // display the first navigation drawer view on app launch
        displayView(0);
    }



    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }
    public void resetToHomeScreen() {

        FragmentManager fManager = getSupportFragmentManager();
        fManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
    public void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0: {
                resetToHomeScreen();
                fragment = new FragmentHome();
                pushFragment(fragment, false);
            }
                break;
            case 1: {
                resetToHomeScreen();
                fragment = new FragmentAboutus();
                pushFragment(fragment, true);
            }
                break;
            case 2: {
                resetToHomeScreen();
                fragment = new FragmentContactus();
                pushFragment(fragment, true);
            }
                break;
            case 3: {
                resetToHomeScreen();
                fragment = new FragmentFeedback();
                pushFragment(fragment, true);
            }
                break;
            case 4: {
                fragment = new FragmentDetails();
                pushFragment(fragment, true);
            }
                break;
            default:
                break;
        }


    }

    private void pushFragment(Fragment fragment, boolean state){
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            if (state) {
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
            }
            fragmentTransaction.commit();
        }
    }

    public void update(String title){
        headerTitle.setText(title);
    }
}