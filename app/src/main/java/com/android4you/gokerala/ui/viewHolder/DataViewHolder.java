package com.android4you.gokerala.ui.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android4you.gokerala.R;


/**
 * Created by Manu on 11/14/2017.
 */

public class DataViewHolder extends RecyclerView.ViewHolder {
    public final TextView titleTV;
    public final ImageView iconMV;
    public final LinearLayout itemService;


    public DataViewHolder(View view) {
        super(view);
        titleTV =  view.findViewById(R.id.categoryTitle);
        iconMV =  view.findViewById(R.id.iconIV);
        itemService =  view.findViewById(R.id.onClickListener);
    }

}
