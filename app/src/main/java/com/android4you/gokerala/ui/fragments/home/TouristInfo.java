package com.android4you.gokerala.ui.fragments.home;


import com.android4you.gokerala.ui.models.SpotModel;

import java.util.ArrayList;

/**
 * Created by Manu on 11/16/2017.
 */

public class TouristInfo {


    private ArrayList<SpotModel> spotModels = new ArrayList<SpotModel>();


    public ArrayList<DistrictModel> getDistrictsList(String[] districts) {
        ArrayList<DistrictModel> districtModels = new ArrayList<DistrictModel>();
        int i = 1;
        for (String str : districts) {
            DistrictModel districtModel = new DistrictModel();
            districtModel.setId(i);
            districtModel.setTitle(str);
            districtModels.add(districtModel);
            i++;
        }
        return districtModels;
    }

    public ArrayList<SpotModel> getSpotList(int districtid, int categoryid) {
        //   Log.e("District Id ==>", districtid+"");
        // Log.e("Category Id===>",categoryid+"");
        spotModels.clear();
        prepareArrayLits();
        Criteria criteria = new FilterCriteria();
        return criteria.meetCriteria(spotModels, districtid, categoryid);
    }

    public void prepareArrayLits() {
        //Beaches
        AddObjectToList(1, "Shanghumukham Beach", 1, 1);
        AddObjectToList(2, "Varkala Beach", 1, 1);
        AddObjectToList(3, "Hawa Beach", 1, 1);
        AddObjectToList(4, "Kovalam Beach", 1, 1);
        AddObjectToList(5, "Chowara Beach", 1, 1);
        AddObjectToList(6, "Thumba Beach", 1, 1);
        AddObjectToList(7, "Puthenthoppu Beach", 1, 1);
        AddObjectToList(7, "Ashoka Beach", 1, 1);
        AddObjectToList(8, "Vizhinjam Lighthouse", 1, 1);
        AddObjectToList(12, "Kollam Beach", 2, 1);
        AddObjectToList(12, "Thirumullavaram Beach", 2, 1);
        AddObjectToList(12, "Neendakara Port", 2, 1);
        AddObjectToList(12, "Thangassery", 2, 1);
        AddObjectToList(12, "Alleppey Beach", 4, 1);
        AddObjectToList(12, "Marari Beach", 4, 1);
        AddObjectToList(12, "Kumarakom", 5, 1);

        AddObjectToList(12, "Cherai Beach", 7, 1);
        AddObjectToList(12, "Vypeen Island Zone", 7, 1);
        AddObjectToList(12, "Marine Drive", 7, 1);
        AddObjectToList(12, "Andhakaranazhi Beach", 7, 1);
        AddObjectToList(12, "Puthuvype Beach", 7, 1);










        //Backwaters
        AddObjectToList(20, "Thiruvallam Backwaters", 1, 2);
        AddObjectToList(20, "Poovar Island", 1, 2);
        AddObjectToList(20, "Munroe Island", 2, 2);
        AddObjectToList(20, "Ashtamudi Lake", 2, 2);
        AddObjectToList(20, "Sasthamcotta Lake", 2, 2);
        AddObjectToList(20, "Kakki Reservoir", 3, 2);
        AddObjectToList(12, "Kuttanad Backwaters", 4, 2);
        AddObjectToList(12, "Pathiramanal", 4, 2);
        AddObjectToList(12, "Vembanad Lake", 4, 2);
        AddObjectToList(12, "Kayamkulam Lake", 4, 2);
        AddObjectToList(12, "Nehru Trophy Snake Boat Race", 4, 2);
        AddObjectToList(12, "Island of Pathiramanal", 5, 2);
        AddObjectToList(12, "Sita Devi Lake",6,2);


        //wildlife
        AddObjectToList(20, "Peppara Wildlife Sanctuary", 1, 3);
        AddObjectToList(20, "Neyyar Wildlife Sanctuary", 1, 3);
        AddObjectToList(20, "Agasthyakoodam", 1, 3);
        AddObjectToList(20, "Shenduruny Wildlife Sanctuary", 2, 3);
        AddObjectToList(20, "Konni Forest Reserve", 3, 3);
        AddObjectToList(20, "Kumarakom Bird Sanctuary", 4, 3);
        AddObjectToList(20, "Idukki Wildlife Sanctuary", 6, 3);
        AddObjectToList(20, "Kurinjimala Sanctuary", 6, 3);
        AddObjectToList(20, "Thattekkad Bird Sanctuary", 6, 3);
        AddObjectToList(20, "Periyar Tiger Reserve", 6, 3);
        AddObjectToList(20, "Chinnar Wildlife Sanctuary", 6, 3);
        AddObjectToList(20, "Eravikulam National Park", 6, 3);
        AddObjectToList(20,"Marayoor Sandalwood", 6, 3);


        //hillstation
        AddObjectToList(20, "Ponmudi", 1, 4);
        AddObjectToList(20, "Bonacaud", 1, 4);
        AddObjectToList(20, "Thenmala", 2, 4);
        AddObjectToList(20, "Mayyanad ", 2, 4);
        AddObjectToList(20, "Gavi ", 3, 4);
        AddObjectToList(20, "Charalkunnu ", 3, 4);
        AddObjectToList(20, "Elaveezhapoonchira ", 5, 4);
        AddObjectToList(20, "Erumeli ", 5, 4);
        AddObjectToList(20, "Iillikal kallu ", 5, 4);
        AddObjectToList(20, "Nadukani  ", 5, 4);
        AddObjectToList(20, "Kottathavalam ", 5, 4);
        AddObjectToList(20, "Nattakom and Panachikad ", 5, 4);
        AddObjectToList(20, "Kannadipara", 5, 4);

        AddObjectToList(20, "Munnar", 6, 4);
        AddObjectToList(20,"Mattuppetty",6,4);
        AddObjectToList(20, "Meesapulimala", 6, 4);
        AddObjectToList(20, "Nedumkandam Hills", 6, 4);
        AddObjectToList(20, "Ramakkalmedu", 6, 4);
        AddObjectToList(20, "Parunthum Para", 6, 4);
        AddObjectToList(20, "Thrissanku Hills", 6, 4);
        AddObjectToList(20, "Nadukani", 6, 4);
        AddObjectToList(20, "Kalavari Mount", 6, 4);
        AddObjectToList(20, "Nadukani", 6, 4);
        AddObjectToList(20, "Hill View Park", 6, 4);
        AddObjectToList(20, "Thumapachi Calveri Samuchayam", 6, 4);
        AddObjectToList(20, "Panchalimedu", 6, 4);
        AddObjectToList(20, "Palkulamedu", 6, 4);
        AddObjectToList(20, "Kolukkumalai Tea Estate", 6, 4);
        AddObjectToList(20, "Peermede", 6, 4);
        AddObjectToList(20, "Anakkara", 6, 4);
        AddObjectToList(20, "Murikkady", 6, 4);
        AddObjectToList(20,"Marayoor",6,4);
        AddObjectToList(20,"Devikulam",6,4);
        AddObjectToList(20,"Vandanmedu",6,4);
        AddObjectToList(20,"Pullumedu",6,4);
        AddObjectToList(20,"Pothamedu",6,4);
        AddObjectToList(20, "Thekkady",6,4);
        AddObjectToList(20,"kannan Deven Hills",6,4);
        AddObjectToList(20, "Kumaly",6,4);


        //waterfall
        AddObjectToList(20, "Vazhvanthol Waterfalls", 1, 5);
        AddObjectToList(20, "Palaruvi Waterfalls", 2, 5);
        AddObjectToList(20, "Kumbhavurutty Waterfalls", 2, 5);
        AddObjectToList(20, "Perunthenaruvi Waterfalls", 3, 5);
        AddObjectToList(20, "Aruvikkuzhy Waterfalls", 3, 5);
        AddObjectToList(20, "Marmala Waterfall", 5, 5);
        AddObjectToList(20, "Kattikkayam Waterfall", 5, 5);
        AddObjectToList(20, "Aruvikkachal Waterfall", 5, 5);
        AddObjectToList(20, "Thommankuthu Falls", 6, 5);
        AddObjectToList(20, "Cheeyappara Waterfall", 6, 5);
        AddObjectToList(20, "Keezharkuthu Falls", 6, 5);
        AddObjectToList(20, "Anchurili Waterfalls", 6, 5);
        AddObjectToList(20, "Valara Waterfalls", 6, 5);
        AddObjectToList(20, "Njandirukki Waterfalls", 6, 5);
        AddObjectToList(20, "Aruvikkachal Waterfall", 6, 5);
        AddObjectToList(20, "Cheruthoni Dam, Idukki", 6, 5);
        AddObjectToList(20, "Idukki Arch Dam", 6, 5);
        AddObjectToList(20, "Kulamavu Dam", 6, 5);
        AddObjectToList(20, "Ponmudi Dam", 6, 5);
        AddObjectToList(20, "Anayirangal Dam Reservoir", 6, 5);
        AddObjectToList(20,"Peechi Dam", 7,5);


        //Museum
        AddObjectToList(9, "Napier Museum", 1, 6);
        AddObjectToList(10, "Priyadarshini Space Planetarium", 1, 6);
        AddObjectToList(11, "Shri Chitra Art Gallery", 1, 6);
        AddObjectToList(12, "Kuthiramalika Palace Museum", 1, 6);
        AddObjectToList(13, "Natural History Museum", 1, 6);
        AddObjectToList(14, "Kerala Science and Technology Museum", 1, 6);
        AddObjectToList(15, "Chacha Nehru Children’s Museum", 1, 6);
        AddObjectToList(16, "Forest Department’s Natural History Museum", 1, 6);
        AddObjectToList(17, "Legislative Museum", 1, 6);
        AddObjectToList(18, "Keralam - Museum of History and Heritage", 1, 6);
        AddObjectToList(19, "LaGallery360", 1, 6);
        AddObjectToList(20, "Observatory", 1, 6);
        AddObjectToList(21, "Police Museum", 2, 6);
        AddObjectToList(22, "Kottarakkara Kathakali Museum", 2, 6);
        AddObjectToList(23, "Paaramparya Museum", 2, 6);
        AddObjectToList(20, "Muloor Smarakam", 3, 6);
        AddObjectToList(20, "Mannadi ( folk Art Centre )", 3, 6);
        AddObjectToList(20, "Bay Island Driftwood Museum", 4, 6);
        AddObjectToList(20, "International Coir Museum", 4, 6);
        AddObjectToList(20, "Mozart Art Gallery", 5, 6);
        AddObjectToList(20, "Clay Art Cafe", 5, 6);
        AddObjectToList(20, "Bay Island Driftwood Museum", 5, 6);
        AddObjectToList(20, "Discs & Machines – Sunny’s Gramophone Museum", 5, 6);
        AddObjectToList(20,"Tea Museum",6,6);
        AddObjectToList(20, "Ashley Bunglow",6,6);

        AddObjectToList(20, "Indian Naval Maritime Museum",7,6);
        AddObjectToList(20, "Kerala Folklore Theatre Museum",7,6);
        AddObjectToList(20, "Cochin Cultural Centre",7,6);
        AddObjectToList(20, "Museum of Art and Kerala History",7,6);
        AddObjectToList(20, "Kerala Kathakali Centre",7,6);
        AddObjectToList(20, "Archaeological Museum",7,6);
        AddObjectToList(20, "Forest Museum",7,6);
        AddObjectToList(20, "M. N. F. GALLERY OF PAINTINGS & SCULPTURES",7,6);
        AddObjectToList(20, "CHITRAM ART GALLERY",7,6);


        //Palace
        AddObjectToList(20, "The Kanakakunnu Palace", 1, 7);
        AddObjectToList(20, "Kuthiramalika (Puthenmalika) Palace", 1, 7);
        AddObjectToList(20, "Padmanabhapuram Palace", 1, 7);
        AddObjectToList(20, "Kowdiar Palace", 1, 7);
        AddObjectToList(20, "Koyikkal Palace(Nedumangad Palace)", 1, 7);
        AddObjectToList(20, "Thevally Palace", 2, 7);
        AddObjectToList(20, "Pandalam Palace", 3, 7);
        AddObjectToList(20, "Krishnapuram Palace", 4, 7);
        AddObjectToList(20, "Poonjar Palace", 5, 7);
        AddObjectToList(20,"Ammachi Kottaram", 6,7);
        AddObjectToList(20,"Hill Palace of Tripunithura", 7,7);
        AddObjectToList(20,"Mattancherry Palace (Dutch Palace)", 7,7);
        AddObjectToList(20,"Bolgatty Palace", 7,7);
        AddObjectToList(20,"Paliam Palace,Chendamangalam", 7,7);




        //Temple
        AddObjectToList(20, "Sri Padmanabhaswamy Temple", 1, 8);
        AddObjectToList(20, "Pazhavangadi Ganapathy Temple", 1, 8);
        AddObjectToList(20, "Attukal Bhagavathy Temple", 1, 8);
        AddObjectToList(20, "Janardanaswamy Temple", 1, 8);
        AddObjectToList(20, "Kottarakkara Mahaganapathi Temple", 2, 8);
        AddObjectToList(20, "Kottukal Rock-Cut Temple", 2, 8);
        AddObjectToList(20, "Rameshwara Temple", 2, 8);
        AddObjectToList(20, "Ochira ", 2, 8);
        AddObjectToList(20, "Kulathupuzha ", 2, 8);
        AddObjectToList(20, "Ariankavu ", 2, 8);
        AddObjectToList(20, "Pattazhi", 2, 8);
        AddObjectToList(20, "Achencoil", 2, 8);
        AddObjectToList(20, "Sastha Temple", 2, 8);
        AddObjectToList(20, "Sree Venkatachalapathy Temple", 2, 8);
        AddObjectToList(20, "Sabarimala Sri Dharmasastha Temple", 3, 8);
        AddObjectToList(20, "Aranmula Parthasarathy Temple", 3, 8);
        AddObjectToList(20, "Kaviyoor Mahadeva Temple", 3, 8);
        AddObjectToList(20, "Bhagavathy Temple", 3, 8);
        AddObjectToList(20, "Malayalappuzha Devi Temple", 3, 8);
        AddObjectToList(20, "Sree Vallabha Temple", 3, 8);
        AddObjectToList(20, "Valiyakoikal Temple", 3, 8);
        AddObjectToList(20, "Sri Subrahmanya Swamy Temple", 3, 8);
        AddObjectToList(20, "Kodumon Chilanthiyambalam Temple", 3, 8);
        AddObjectToList(20, "Kadamanitta Devi Temple", 3, 8);
        AddObjectToList(20, "Kaviyoor Rock Cut Temple", 3, 8);
        AddObjectToList(20, "Karumadi", 4, 8);
        AddObjectToList(20, "Pandavan Rock", 4, 8);
        AddObjectToList(20, "Ambalapuzha Temple", 4, 8);
        AddObjectToList(20, "Mannarasala Temple", 4, 8);
        AddObjectToList(20, "Mullakal Bhagvathy Temple", 4, 8);
        AddObjectToList(20, "Chettikulangara Devi Temple", 4, 8);
        AddObjectToList(20, "Chakkulathukavu Temple", 4, 8);
        AddObjectToList(20, "Kanichukulangara Devi Temple", 4, 8);
        AddObjectToList(20, "Thuravoor Mahakshethram", 4, 8);
        AddObjectToList(20, "Thiruvanvandoor Pambanaiappan Temple", 4, 8);
        AddObjectToList(20, "Subramania Swami Temple", 5, 8);
        AddObjectToList(20, "Thirunakkara Mahadev Temple", 5, 8);
        AddObjectToList(20, "Malliyoor Sri Maha Ganapathi Temple", 5, 8);
        AddObjectToList(20, "Panachikkadu Dakshina Mookambika Saraswathi Temple", 5, 8);
        AddObjectToList(20, "Ganapathiyar Kovil Temple", 5, 8);
        AddObjectToList(20, "Kumaranalloor Bhagavathy Temple", 5, 8);
        AddObjectToList(20, "Vaikom Mahadeva Temple", 5, 8);
        AddObjectToList(20, "Pallippurathu Kavu", 5, 8);
        AddObjectToList(20, "Thiruvarpu temple", 5, 8);
        AddObjectToList(20,"Mangala Devi Temple",6,8);

        AddObjectToList(20,"Sri Chottanikkara Bhagavathy Temple",7,8);
        AddObjectToList(20,"Paavakkulam Mahadeva Temple",7,8);
        AddObjectToList(20,"Ernakulam Siva Temple",7,8);
        AddObjectToList(20,"Erattakulangara Sri Mahadeva Temple",7,8);
        AddObjectToList(20,"Thenali Temple",7,8);









        //Church
        AddObjectToList(20, "Manacaud Church", 1, 9);
        AddObjectToList(20, "St. Mary's Cathedral, Pattom", 1, 9);
        AddObjectToList(20, "Pullichira Church", 2, 9);
        AddObjectToList(20, "St Thomas Syrian Catholic Church", 2, 9);
        AddObjectToList(20, "Holy Cross Church", 2, 9);
        AddObjectToList(20, "St.George Orthodox Church", 3, 9);
        AddObjectToList(20, "Paliakara Church", 3, 9);
        AddObjectToList(20, "St Thomas Ecumenical Church", 3, 9);
        AddObjectToList(20, "Manjinikkara Church", 3, 9);
        AddObjectToList(20, "Malankara Marthoma Syrian Church", 3, 9);
        AddObjectToList(20, "Niranam", 3, 9);
        AddObjectToList(20, "St. Mary's Forane Church", 4, 9);
        AddObjectToList(20, "St. Andrew's Basilica Arthunkal", 4, 9);
        AddObjectToList(20, "Edathua Church", 4, 9);
        AddObjectToList(20, "St. Mary?s Orthodox Church", 5, 9);
        AddObjectToList(20, "St Mary's Forane Church", 5, 9);
        AddObjectToList(20, "St mary’s knanaya church", 5, 9);
        AddObjectToList(20,"Pattumala Palli", 6,9);
        AddObjectToList(20, "Vagamon Kurusumala",6,9);
        AddObjectToList(20,"Edappally Church Complex", 7,9);
        AddObjectToList(20,"Kadamattom Chruch", 7,9);
        AddObjectToList(20,"St. Mary's Cathedral Basilica, Ernakulam", 6,9);
        AddObjectToList(20,"Little Flower Church", 7,9);
        AddObjectToList(20,"St. Francis Church, Fort Cochin", 7,9);
        AddObjectToList(20,"Malayattoor", 7,9);



        //Mosque
        AddObjectToList(20, "Jama Masjid, Beemapally", 1, 10);
        AddObjectToList(20, "Palayam Juma Masjid", 1, 10);
        AddObjectToList(20, "Thazhathangady Juma Masjid", 5, 10);
        AddObjectToList(20,"Kanjiramattam Mosque",7,10);

        //other
        AddObjectToList(20, "Vikram Sarabhai Space Center", 1, 11);
        AddObjectToList(20, "College of Fine Arts", 1, 11);
        AddObjectToList(20, "Greenfield Stadium", 1, 11);
        AddObjectToList(20, "Magic Planet Theme Park", 1, 11);
        AddObjectToList(20, "Veli Tourist Village", 1, 11);
        AddObjectToList(20, " British Residency", 2, 11);
        AddObjectToList(20, "Jatayupara", 2, 11);
        AddObjectToList(20, "Punalur Suspension Bridge", 2, 11);
        AddObjectToList(20, "Mahatma Gandhi Park", 2, 11);
        AddObjectToList(20, "Adventure Park", 2, 11);
        AddObjectToList(20, "Elephant Training Center, Konni", 3, 11);
        AddObjectToList(20, "Palakarai Aqua Tourism Farm", 5, 11);
        AddObjectToList(20, "Puthupally Elephant Point", 5, 11);

        AddObjectToList(20,"Pallipuram Fort",7,11);
        AddObjectToList(20,"Wonderla Amusement Park",7,11);
        AddObjectToList(20,"Kodanad Elephant Training Center",7,11);
        AddObjectToList(20,"Changampuzha Park",7,11);

    }

    // Ad one item into the Array List
    public void AddObjectToList(int id, String title, int districtid, int categoryid) {
        SpotModel bean = new SpotModel(id, title, districtid, categoryid);
        spotModels.add(bean);
    }


}
