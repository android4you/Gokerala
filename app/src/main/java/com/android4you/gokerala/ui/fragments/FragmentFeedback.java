package com.android4you.gokerala.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.gokerala.R;
import com.android4you.gokerala.ui.HomeActivity;


/**
 * Created by Manu on 11/14/2017.
 */

public class FragmentFeedback extends Fragment {
    private HomeActivity homeActivity;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView!=null){
            return rootView;
        }
        rootView = inflater.inflate(R.layout.fragment_feedback, container, false);
        homeActivity = (HomeActivity) getActivity();
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        homeActivity.update(getString(R.string.nav_item_feedback));
    }
    }
