package com.android4you.gokerala.ui.fragments.home;

/**
 * Created by Manu on 11/16/2017.
 */

public class DistrictModel {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private int id;
    private String title;
}
