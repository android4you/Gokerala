package com.android4you.gokerala.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.android4you.gokerala.R;


/**
 * Created by Manu on 11/15/2017.
 */

public class GalleryItemView extends RecyclerView {

    private static int NORMAL_COLOR = Color.GRAY;
    private static int SELECTED_COLOR = Color.RED;
    private static int NORMAL_TEXT_COLOR = Color.BLACK;
    private static int SELECTED_TEXT_COLOR = Color.WHITE;


    public GalleryItemView(Context context) {
        super(context);
        init(null);
    }

    public GalleryItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public GalleryItemView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomColor);
            String selectedColor = a.getString(R.styleable.CustomColor_selectedcolorCode);
            String unSelectedColor = a.getString(R.styleable.CustomColor_unSelectedcolorCode);
            String textSelectedColor = a.getString(R.styleable.CustomColor_textUnSelectedColorCode);
            String textUnSelectedColor = a.getString(R.styleable.CustomColor_textUnSelectedColorCode);
            if (selectedColor != null) {
                setSelectedColor(Color.parseColor(selectedColor));
                setBackgroundResource(R.drawable.hightlightimage);
            }
            if (unSelectedColor != null) {
                setUnSelectedColor(Color.parseColor(unSelectedColor));
                setBackgroundResource(R.drawable.unhightlightimage);
            }
            if (textSelectedColor != null) {
                setTextSelectedColor(Color.parseColor(textSelectedColor));
            }
            if (textUnSelectedColor != null) {
                setTextUnSelectedColor(Color.parseColor(textUnSelectedColor));
            }
            a.recycle();
        }
    }

    public static int getSelectedColor() {
        return SELECTED_COLOR;
    }

    public void setSelectedColor(int color) {
        SELECTED_COLOR = color;
    }

    public static int getUnSelectedColor() {
        return NORMAL_COLOR;
    }

    public void setUnSelectedColor(int color) {
        NORMAL_COLOR = color;
    }


    public static int getTextSelectedColor() {
        return SELECTED_TEXT_COLOR;
    }

    public static void setTextSelectedColor(int color) {
        SELECTED_TEXT_COLOR = color;
    }

    public static int getTextUnSelectedColor() {
        return NORMAL_TEXT_COLOR;
    }

    public static void setTextUnSelectedColor(int color) {
        NORMAL_TEXT_COLOR = color;
    }
}