package com.android4you.gokerala.ui.fragments.home;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.gokerala.R;
import com.android4you.gokerala.ui.HomeActivity;
import com.android4you.gokerala.ui.adapter.DataAdapter;
import com.android4you.gokerala.ui.models.ItemVerticalModel;
import com.android4you.gokerala.ui.models.SpotModel;
import com.android4you.gokerala.ui.widget.GalleryAdapter;
import com.android4you.gokerala.ui.widget.GalleryItemView;
import com.android4you.gokerala.ui.widget.GalleryLayoutManager;
import com.android4you.gokerala.ui.widget.InfiniteTabAdapter;
import com.android4you.gokerala.ui.widget.InfiniteTabView;
import com.android4you.gokerala.ui.widget.TabLayoutManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manu on 11/13/2017.
 */

public class FragmentHome extends Fragment implements DataAdapter.OnItemSelectionListener {
    private InfiniteTabView infiniteTabView;
    private GalleryItemView recyclerViewVertical;
    private RecyclerView recyclerViewHome;
    private List<ItemVerticalModel> verticalModels;
    private HomeActivity homeActivity;
    TabLayoutManager tabLayoutManager;
    InfiniteTabAdapter infiniteTabAdapter;
    GalleryLayoutManager galleryLayoutManager;
    GalleryAdapter galleryAdapter;
    TouristInfo touristInfo;

    private ArrayList<SpotModel> spotModelLists = new ArrayList<>();
    private int districtID = 1;
    private View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView!=null){
            return rootView;
        }
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        homeActivity = (HomeActivity) getActivity();

        infiniteTabView =  rootView.findViewById(R.id.infiniteTabs);
        recyclerViewVertical =  rootView.findViewById(R.id.recyclerViewVertical);
        recyclerViewHome =  rootView.findViewById(R.id.recyclerViewHome);
        infiniteTabView.setUnSelectedColor(Color.parseColor("#2c9220"));
        infiniteTabView.setSelectedColor(Color.parseColor("#133e0d"));
        //  infiniteTabView.setTextSelectedColor(Color.RED);

        recyclerViewVertical.setUnSelectedColor(Color.parseColor("#d0dcdc"));
        recyclerViewVertical.setSelectedColor(Color.parseColor("#FFFFFF"));
        touristInfo = new TouristInfo();
        prepareArrayLits();
        setupHorizontalTabs();

        // setupData();

//        for(SpotModel spotModel: spotModelLists){
//            Log.e("Title ===>   ", spotModel.getTitle());
//        }
        return rootView;
    }


    private void setupHorizontalTabs() {
        final String[] districts = getActivity().getResources().getStringArray(R.array.districts);
        final List<DistrictModel> listdistricts = touristInfo.getDistrictsList(districts);
        tabLayoutManager = new TabLayoutManager();
        infiniteTabView.setOnFlingListener(null);
        tabLayoutManager.attach(infiniteTabView, listdistricts);

        tabLayoutManager.setOnItemSelectedListener(new TabLayoutManager.OnItemSelectedListener() {
            @Override
            public void onItemSelected(RecyclerView recyclerView, View item, int position) {
                Log.e("ID =====>  ", listdistricts.get(position).getId()+"");
                districtID = listdistricts.get(position).getId();
                setupVerticalCategory(listdistricts.get(position).getId());

            }
        });
        infiniteTabAdapter = new InfiniteTabAdapter(listdistricts);
        infiniteTabAdapter.setOnItemClickListener(new InfiniteTabAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                infiniteTabView.smoothScrollToPosition(position);
            }
        });
        infiniteTabView.setAdapter(infiniteTabAdapter);
    }

    private void setupVerticalCategory(final int districtid) {
        galleryLayoutManager = new GalleryLayoutManager(GalleryLayoutManager.VERTICAL);
        galleryLayoutManager.attach(recyclerViewVertical, verticalModels.size());
        galleryLayoutManager.setCallbackInFling(false);
        galleryLayoutManager.setOnItemSelectedListener(new GalleryLayoutManager.OnItemSelectedListener() {
            @Override
            public void onItemSelected(RecyclerView recyclerView, View item, int position) {
             //   Log.e("Psot=== > ", position + "");
                int categoryId = verticalModels.get(position).getId();
                setupData(districtID, categoryId);

            }
        });

        galleryAdapter = new GalleryAdapter(verticalModels);
        galleryAdapter.setOnItemClickListener(new GalleryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                recyclerViewVertical.smoothScrollToPosition(position);
            }
        });
        recyclerViewVertical.setAdapter(galleryAdapter);
    }

    private void setupData(int districtid, int categoryId) {
        spotModelLists.clear();

        spotModelLists = touristInfo.getSpotList(districtid, categoryId);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerViewHome.setLayoutManager(horizontalLayoutManagaer);
        DataAdapter dataAdapter = new DataAdapter(getActivity(), spotModelLists, this);
        recyclerViewHome.setAdapter(dataAdapter);
//        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(recyclerViewHome.getContext(),
//                DividerItemDecoration.VERTICAL);
//        Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.horizontal_divider);
//        horizontalDecoration.setDrawable(horizontalDivider);
//        recyclerViewHome.addItemDecoration(horizontalDecoration);
    }


    public void prepareArrayLits() {
        verticalModels = new ArrayList<ItemVerticalModel>();
        AddObjectToList(1, R.mipmap.icon_beach, "Beaches");
        AddObjectToList(2, R.mipmap.icon_backwater, "Backwater");
        AddObjectToList(3, R.mipmap.icon_forest, "Wildlife");
        AddObjectToList(4, R.mipmap.icon_hillstation, "Hill station");
        AddObjectToList(5, R.mipmap.icon_waterfall, "Water fall");
        AddObjectToList(6, R.mipmap.icon_museum, "Museum");
        AddObjectToList(7, R.mipmap.icon_palace, "Palaces");
        AddObjectToList(8, R.mipmap.icon_temple, "Temples");
        AddObjectToList(9, R.mipmap.icon_church, "Church");
        AddObjectToList(10, R.mipmap.icon_mosque, "Mosque");
        AddObjectToList(11, R.mipmap.icon_monuments, "Monuments");

    }

    // Add one item into the Array List
    public void AddObjectToList(int id, int image, String title) {
        ItemVerticalModel bean = new ItemVerticalModel();
        bean.setId(id);
        bean.setIcon(image);
        bean.setTitle(title);
        verticalModels.add(bean);
    }


    @Override
    public void onResume() {
        super.onResume();
        homeActivity.update(getString(R.string.nav_item_home));
    }

    @Override
    public void setSelectionListener(int index, Object str) {
        homeActivity.displayView(4);
    }
}
