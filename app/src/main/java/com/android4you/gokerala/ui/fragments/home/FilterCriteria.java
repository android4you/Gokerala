package com.android4you.gokerala.ui.fragments.home;


import com.android4you.gokerala.ui.models.SpotModel;

import java.util.ArrayList;

/**
 * Created by Manu on 11/16/2017.
 */

public class FilterCriteria implements Criteria {
    @Override
    public ArrayList<SpotModel> meetCriteria(ArrayList<SpotModel> spotModels, int districtId, int categoryId) {
        ArrayList<SpotModel> spotModelList = new ArrayList<SpotModel>();

        for (SpotModel spotModel : spotModels) {
            if(spotModel.getCategoryid()==categoryId && spotModel.getDistrictid() == districtId){
                spotModelList.add(spotModel);
            }
        }
        return spotModelList;
    }
}
