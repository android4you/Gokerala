package com.android4you.gokerala.ui.fragments.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android4you.gokerala.R;
import com.android4you.gokerala.ui.HomeActivity;


/**
 * Created by Manu on 11/17/2017.
 */

public class FragmentDetails extends Fragment {

    HomeActivity homeActivity;
    private TextView descriptionTV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details, container, false);
        homeActivity = (HomeActivity) getActivity();
        descriptionTV = rootView.findViewById(R.id.descriptionTV);
        descriptionTV.setText(str);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        homeActivity.update("Hill Station");
    }


    String str = "The curved dam across Periyar River is one of the popular tourist attractions in Idukki. It is a dam of distinctions – one of India’s tallest dams, Asia’s first arch dam and world’s second arch dam.\n" +
            "\n" +
            "Idukki Arch Dam was built by State’s Electricity Board for power generation. They also built two other dams in the vicinity. Together the three dams create a reservoir of 60 sq kms, big enough to be considered as an artificial lake.\n" +
            "\n" +
            "Idukki wildlife sanctuary is located along this waterfront." +
            "Spreading along the banks of Idukki Dam reservoir, this reserve is home to a range of species such as elephant, deer, boar, bison, cobra and giant squirrel and diverse birds and snakes.\n" +
            "\n" +
            "The sanctuary is surrounded on three sides by the lake of Idukki Dam. Water attracts elephant herds and other animals, which can be viewd from the lake.\n" +
            "\n" +
            "Occupying about 77 sq kms, this sanctuary is smaller compared to other sanctuaries such as Chinnar and Periyar. Located in a region of heavy rainfall, plant and animal life thrives in the dense forests of the sanctuary.\n" +
            "\n" +
            "The nearest major town is Thodupuzha at 40 kms." +
            "Chinnar Wildlife Sanctuary is located in a rain shadow region. Thorny vegetation distinguishes Chinnar from other Kerala Wildlife Sanctuaries.\n" +
            "\n" +
            "Chinnar is rich in animal life. The 34 species of mammal inhabitants include elephants, panthers, tigers, bisons, deer and varieties of monkeys. A commendable 245 species of birds in addition to 52 species of reptiles are seen here.\n" +
            "\n" +
            "Chinnar protects globally threatened and rare species. Grizzled giant squirrel, Nilgiri Tahr and Star Tortoise contribute in turning Chinnar into one of the major Idukki tourist attractions.\n" +
            "\n" +
            "Thoovanam waterfall is located within Chinnar Sanctuary.Treks to the waterfall start from Karimutty Forest Station and requires the help of local guides.\n" +
            "\n" +
            "The sanctuary extends on either side of Munnar to Udumalpet road. Marayoor, famous for sandal forest, Stone Age burial chambers and ancient cave paintings are along this road, further from the sanctuary.\n" +
            "\n" +
            "Chinnar is about 60 kms from Munnar.";
}
