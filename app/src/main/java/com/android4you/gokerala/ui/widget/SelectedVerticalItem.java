package com.android4you.gokerala.ui.widget;

import android.view.View;

import com.android4you.gokerala.R;


/**
 * Created by Manu on 11/15/2017.
 */

public class SelectedVerticalItem implements GalleryLayoutManager.ItemSelectionHighlightListener {
    @Override
    public void highlightItem(GalleryLayoutManager layoutManager, View item, float fraction) {
        item.setPivotX(item.getWidth() / 2.f);
        item.setPivotY(item.getHeight() / 2.0f);
        //   float scale = 1 - 0.2f * Math.abs(fraction);

        if (fraction == 0.0) {
            item.setBackgroundResource(R.drawable.hightlightimage);
            item.setPadding(30,0,0,0);
        } else {
            item.setBackgroundResource(R.drawable.unhightlightimage);
        }
    }
}