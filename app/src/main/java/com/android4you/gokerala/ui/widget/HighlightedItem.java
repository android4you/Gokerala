package com.android4you.gokerala.ui.widget;

import android.view.View;

import static com.android4you.gokerala.ui.widget.InfiniteTabView.getSelectedColor;
import static com.android4you.gokerala.ui.widget.InfiniteTabView.getUnSelectedColor;

/**
 * Created by Manu on 11/13/2017.
 */

public class HighlightedItem implements TabLayoutManager.ItemSelectionHighlightListener {
    @Override
    public void highlightItem(TabLayoutManager layoutManager, View item, float fraction) {
        item.setPivotX(item.getWidth() / 2.f);
        item.setPivotY(item.getHeight() / 2.0f);
        //   float scale = 1 - 0.2f * Math.abs(fraction);

        if (fraction == 0.0) {
            item.setBackgroundColor(getSelectedColor());
        } else {
            item.setBackgroundColor(getUnSelectedColor());
        }
    }
}