package com.android4you.gokerala.ui.fragments.home;


import com.android4you.gokerala.ui.models.SpotModel;

import java.util.ArrayList;

/**
 * Created by Manu on 11/16/2017.
 */

public interface Criteria {
    public ArrayList<SpotModel> meetCriteria(ArrayList<SpotModel> spotModels, int districtId, int categoryId);
}
