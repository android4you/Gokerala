package com.android4you.gokerala.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.gokerala.R;
import com.android4you.gokerala.ui.models.SpotModel;
import com.android4you.gokerala.ui.viewHolder.DataViewHolder;

import java.util.List;

/**
 * Created by Manu on 11/14/2017.
 */

public class DataAdapter extends RecyclerView.Adapter<DataViewHolder> {
    private List<SpotModel> spotModelList;
    private Context context;
    OnItemSelectionListener onItemSelectionListener;

    public DataAdapter(Context context, List<SpotModel> items, OnItemSelectionListener onItemSelectionListener) {
        this.context = context;
        spotModelList = items;
        this.onItemSelectionListener = onItemSelectionListener;

    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_row, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        holder.titleTV.setText(spotModelList.get(position).getTitle());
        holder.titleTV.setTextColor(Color.BLACK);
        //   holder.iconMV.setImageResource(mValues.get(position).getImage());

        holder.itemService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                onItemSelectionListener.setSelectionListener(spotModelList.get(position).getId(), spotModelList.get(position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return spotModelList.size();
    }

    public interface OnItemSelectionListener {
        void setSelectionListener(int index, Object str);
    }
}
