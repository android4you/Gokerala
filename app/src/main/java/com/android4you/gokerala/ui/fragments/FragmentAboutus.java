package com.android4you.gokerala.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android4you.gokerala.R;
import com.android4you.gokerala.ui.HomeActivity;


/**
 * Created by Manu on 11/14/2017.
 */

public class FragmentAboutus extends Fragment{

    private HomeActivity homeActivity;
    private TextView descriptionTV;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView!=null){
            return rootView;
        }
        rootView = inflater.inflate(R.layout.fragment_aboutus, container, false);
        homeActivity = (HomeActivity)getActivity();

        descriptionTV = rootView.findViewById(R.id.descriptionTV);
        descriptionTV.setText(str);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        homeActivity.update(getString(R.string.nav_item_about));
    }

    private String str = "Kerala is a tropical country in the south of India and a perfect nature destination. Kerala offers swaying elephant rides, gentle houseboat cruises along tropical backwaters, jungle trekking, cardamom plantation visits, waterfall visits, mist clad hill stations…. Once you’re in Kerala, you’ll soon discover why National Geographic Traveller voted ‘Kerala one of its ten Paradise Found’.\n" +
            "Munnar hill station is known for its serenity, Kumarakom has emerald stretches of backwaters, Thekkady has exotic flora & fauna, and Kovalam is popular for its pristine beaches. With diverse landscapes, each destination clearly justifies Kerala’s title of ‘God’s Own Country’.";
    }
