package com.android4you.gokerala.ui.models;

/**
 * Created by Manu on 11/16/2017.
 */

public class SpotModel {

    private int id;
    private String title;

    public SpotModel(int id, String title, int districtid, int categoryid) {
        this.id = id;
        this.title = title;
        this.districtid = districtid;
        this.categoryid = categoryid;
    }

    private int districtid;
    private int categoryid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDistrictid() {
        return districtid;
    }

    public void setDistrictid(int districtid) {
        this.districtid = districtid;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }


}
